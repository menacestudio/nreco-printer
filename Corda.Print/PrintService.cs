﻿using NReco.PdfGenerator;
using System.IO;
using System.Net;

namespace Corda.Print
{
    public class PrintService : IPrintService
    {
        public byte[] GeneratePdf(string orientation, string url, bool? showPageNumbers)
        {
            var generator = new HtmlToPdfConverter
            {
                Zoom = (float)1.1,
                Orientation = orientation == "landscape" ? PageOrientation.Landscape : PageOrientation.Portrait
            };

            if (showPageNumbers.HasValue && showPageNumbers.Value)
            {
                generator.PageHeaderHtml =
                    "<div style='text-align:right; font-family:Arial; font-size:10pt; color:#999;'>Page: <span class='page'></span> of <span class='topage'></span></div>";
            }

            var decoded = WebUtility.UrlDecode(url);
            var stream = new MemoryStream();
            generator.GeneratePdfFromFile(decoded, string.Empty, stream);

            return stream.ToArray();
        }
    }
}