﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Corda.Print.Controllers
{
    public class PrintController : ApiController
    {
        // GET api/<controller>?orientation&url&showpagenumbers
        public HttpResponseMessage Get(string orientation, string url, bool? showPageNumbers)
        {
            if (!string.IsNullOrEmpty(url))
            {
                var printService = new PrintService();
                var output = printService.GeneratePdf(orientation, url, showPageNumbers);
                var result = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ByteArrayContent(output)
                };
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"{Guid.NewGuid()}.pdf"
                };
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

                return result;
            }
            return null;
        }
    }
}