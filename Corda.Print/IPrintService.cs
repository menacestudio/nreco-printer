﻿namespace Corda.Print
{
    public interface IPrintService
    {
        byte[] GeneratePdf(string orientation, string url, bool? showPageNumbers);
    }
}